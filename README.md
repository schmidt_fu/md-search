Makes markdown files searchable.

Fast implementation for Florian Schmidt according to this thread;

https://mstdn.social/@schmidt_fu/105259997773586571

-k for key search

-r for range should support date range Y-m-d .. Y-m-d

-R for recursive

-e for quering data like "SELECT sum(key) FROM md WHERE '#key' IN tags"

--id for specifying a specific file

--open to open the file in the default editor

```bash
apt install haxe
haxelib setup
haxelib install hxcpp
haxe build.hxml
bin/md-search
```
