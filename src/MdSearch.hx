import Arguments;
import FileWalker;

class MdSearch {

	static private var instance:MdSearch;

	public var arguments:Arguments;
	public var version:String = "0.1";

	static function main():Void {
		MdSearch.instance = new MdSearch();
	}

	public function new() {
		parseArguments( Sys.args() );

		if ( arguments.version == true || arguments.help == true || arguments.invalidParam != null ) {
			Sys.println( "md-search v" + version );
		}

		if ( arguments.invalidParam != null ) {
			if ( arguments.invalidArgumentToParam == true ) {
				Sys.println( "Invalid argument to parameter: " + arguments.invalidParam + " use -h or --help to display help" );
			} else {
				Sys.println( "Invalid parameter: " + arguments.invalidParam + " use -h or --help to display help" );
			}

			Sys.exit(1);
		}

		if ( arguments.help == true ) {
			Sys.println( "" );
			Sys.println( "-k, --key          Search for a specific key" );
			Sys.println( "-r, --range        Search for a specific (date) range" );
			Sys.println( "-R, --recursive    Recurse into subfolders" );
			Sys.println( "-e, --expression   Evaluate an expression" );
			Sys.println( "-@, --id           Only match file with specific id" );
			Sys.println( "-o, --open         Open file in default editor" );
			Sys.exit(0);
		}

		var lukeFileWalker:FileWalker = new FileWalker( arguments );
		lukeFileWalker.process( "." );
	}

	public function parseArguments( args:Array<String> ):Void {
		var arguments:Arguments = new Arguments();

		var key:String = null;
		var argumentString = null;

		if ( args.length == 0 ) {
			arguments.help = true;
		}

		for ( arg in args ) {
			if ( arg.substring( 0, 2 ) == "--" ) {
				key = arg.substring( 2 );
			} else if ( arg.substring( 0, 1 ) == "-" ) {
				key = arg.substring( 1 );
			}

			if ( key != null ) {
				argumentString = arg;
				if ( key == "k" || key == "key" ) {
					arguments.key = argumentString;

					if ( argumentString == null || argumentString.length == 0 ) {
						arguments.invalidParam = key;
						arguments.invalidArgumentToParam = true;
					}
				} else if ( key == "r" || key == "range" ) {
					arguments.range = argumentString;

					if ( argumentString == null || argumentString.length == 0 ) {
						arguments.invalidParam = key;
						arguments.invalidArgumentToParam = true;
					}
				} else if ( key == "v" || key == "version" ) {
					arguments.version = true;
					key = null;
				} else if ( key == "@" || key == "id" ) {
					arguments.id = argumentString;

					if ( argumentString == null || argumentString.length == 0 ) {
						arguments.invalidParam = key;
						arguments.invalidArgumentToParam = true;
					}
				} else if ( key == "e" || key == "expression" ) {
					arguments.expression = argumentString;

					if ( argumentString == null || argumentString.length == 0 ) {
						arguments.invalidParam = key;
						arguments.invalidArgumentToParam = true;
					}
				} else if ( key == "h" || key == "help" ) {
					arguments.help = true;
					key = null;
				} else if ( key == "R" || key == "recursive" ) {
					arguments.recursive = true;
					key = null;
				} else if ( key == "o" || key == "open" ) {
					arguments.open = true;
					key = null;
				} else {
					arguments.invalidParam = key;
					arguments.help = true;
				}
			}

			if ( key == null && arg != null && arg.length > 0 ) {
				arguments.searchTerm = arg;
			}

			key = null;
			argumentString = null;
		}

		this.arguments = arguments;
	}

}
