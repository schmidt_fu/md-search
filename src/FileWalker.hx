import Arguments;

import sys.io.File;
import sys.FileSystem;

class FileWalker {

	public var arguments:Arguments;
	public var path:String;

	public function new( arguments:Arguments ) {

		this.arguments = arguments;

	}

	public function process( myPath:String = "." ):Void {

		path = FileSystem.fullPath( myPath );

		if ( !FileSystem.isDirectory( path ) || !FileSystem.exists( path ) ) {
			Sys.println( "Invalid path specified" );
			Sys.exit( 1 );
		}

		// Build file list

	}

}