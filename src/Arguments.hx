class Arguments {
	public var key:String;
	public var expression:String;
	public var range:String;
	public var id:String;

	public var recursive:Bool = false;
	public var help:Bool = false;
	public var version:Bool = false;
	public var open:Bool = false;
	public var invalidParam:String;
	public var invalidArgumentToParam:Bool;

	public var searchTerm:String;

	public function new() {}
}