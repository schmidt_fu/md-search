# YAML Inline Block Test
This is a test for the [[YAML Inline Block]] Feature in Markdown.
## Meta
---
title: 'YAML Inline Block Test'
author: Me, Myself & I
date: 2020-11-25
end: 2020-11-30
price: 12.38
tags: [#yaml, #inline, #test]
---
## Subtopic
This is *some* __extra__  ~~markup~~ to irritate the [[Parser]].

And | a | table
--- | --- | ---
to | test | lines
starting | with | dashes

and ending
with
ellipsis
...
